variable "compartment_id" {
  description = "(Required) (Updatable) The ID of the compartment in which to create the bucket."
  type        = string
  default     = ""
}

variable "access_type" {
  # https://www.terraform.io/docs/providers/oci/r/objectstorage_bucket.html#access_type
  description = "(Optional) (Updatable) The type of public access enabled on this bucket. "
  type        = string
  default     = "NoPublicAccess"
}

variable "defined_tags" {
  description = "(Optional) (Updatable) Defined tags for this resource. Each key is predefined and scoped to a namespace"
  type        = map(any)
  default     = {}
}

variable "freeform_tags" {
  description = "(Optional) (Updatable) Free-form tags for this resource. Each tag is a simple key-value pair with no predefined name, type, or namespace."
  type        = map(any)
  default     = {}
}

variable "kms_key_id" {
  description = "(Optional) (Updatable) The OCID of a master encryption key used to call the Key Management service to generate a data encryption key or to encrypt or decrypt a data encryption key."
  type        = string
  default     = null
}

variable "metadata" {
  description = "(Optional) (Updatable) Arbitrary string, up to 4KB, of keys and values for user-defined metadata."
  type        = map(string)
  default     = {}
}

variable "name" {
  description = "(Required) The name of the bucket. Valid characters are uppercase or lowercase letters, numbers, hyphens, underscores, and periods. Bucket names must be unique within an Object Storage namespace."
  type        = string
  default     = ""
}

variable "object_events_enabled" {
  description = "(Optional) (Updatable) Whether or not events are emitted for object state changes in this bucket."
  type        = bool
  default     = false
}

variable "retention_rules" {
  # https://www.terraform.io/docs/providers/oci/r/objectstorage_bucket.html#retention_ruless-1
  description = "(Optional) (Updatable) Creates a new retention rule in the specified bucket."
  type        = any
  default     = []
}

variable "storage_tier" {
  description = "(Optional) The type of storage tier of this bucket."
  type        = string
  default     = ""
}

variable "versioning" {
  description = "(Optional) (Updatable) Set the versioning status on the bucket."
  type        = string
  default     = "Disabled"
}

variable "lifecycle_rule" {
  # https://www.terraform.io/docs/providers/oci/r/objectstorage_object_lifecycle_policy.html#rules
  description = "(Optional) The bucket set of lifecycle rules."
  type        = any
  default     = []
}

variable "replication_policy" {
  # https://www.terraform.io/docs/providers/oci/r/objectstorage_replication_policy.html
  description = "(Optional) The bucket replication policy"
  type        = map(string)
  default     = {}
}
