resource "oci_objectstorage_bucket" "this" {
  compartment_id        = var.compartment_id
  name                  = var.name
  namespace             = data.oci_objectstorage_namespace.this.namespace
  access_type           = var.access_type
  defined_tags          = var.defined_tags
  freeform_tags         = var.freeform_tags
  kms_key_id            = var.kms_key_id
  metadata              = var.metadata
  object_events_enabled = var.object_events_enabled
  storage_tier          = var.storage_tier

  dynamic "retention_rules" {
    for_each = var.retention_rules
    content {
      display_name     = lookup(retention_rules.value, "display_name")
      time_rule_locked = lookup(retention_rules.value, "time_rule_locked", null)
      dynamic "duration" {
        for_each = lookup(retention_rules.value, "duration")
        content {
          time_amount = lookup(duration.value, "time_amount")
          time_unit   = lookup(duration.value, "time_unit")
        }
      }
    }
  }
  versioning = var.versioning
}

resource "oci_objectstorage_object_lifecycle_policy" "this" {
  count     = length(var.lifecycle_rule) == 0 ? 0 : 1
  bucket    = oci_objectstorage_bucket.this.*.name[0]
  namespace = data.oci_objectstorage_namespace.this.namespace

  dynamic "rules" {
    for_each = var.lifecycle_rule
    content {
      action      = lookup(rules.value, "action")
      is_enabled  = lookup(rules.value, "is_enabled")
      name        = lookup(rules.value, "name")
      time_amount = lookup(rules.value, "time_amount")
      time_unit   = lookup(rules.value, "time_unit")
      dynamic "object_name_filter" {
        for_each = lookup(rules.value, "object_name_filter", [])
        content {
          exclusion_patterns = lookup(object_name_filter.value, "exclusion_patterns", [])
          inclusion_patterns = lookup(object_name_filter.value, "inclusion_patterns", [])
          inclusion_prefixes = lookup(object_name_filter.value, "inclusion_prefixes", [])
        }
      }
    }
  }
}

resource "oci_objectstorage_replication_policy" "this" {
  count                               = length(var.replication_policy) == 0 ? 0 : 1
  bucket                              = oci_objectstorage_bucket.this.*.name[0]
  namespace                           = data.oci_objectstorage_namespace.this.namespace
  destination_bucket_name             = var.replication_policy.destination_bucket_name
  destination_region_name             = var.replication_policy.destination_region_name
  name                                = var.replication_policy.name
  delete_object_in_destination_bucket = var.replication_policy.delete_object_in_destination_bucket
}
