module "objectstorage_bucket-name" {
  source         = "git::https://gitlab.com/ganex-cloud/terraform/terraform-oci-object-storage-bucket.git?ref=master"
  compartment_id = module.compartiment_infra.compartment_id
  name           = "bucket-name"
  access_type    = "ObjectReadWithoutList"
  versioning     = "Enabled"
  retention_rule = [
    {
      display_name = "Default"
      duration = [
        {
          time_amount = "10"
          time_unit   = "DAYS"
        }
      ]
    }
  ]

  lifecycle_rule = [
    {
      name        = "Default"
      action      = "ARCHIVE"
      is_enabled  = "true"
      time_amount = "10"
      time_unit   = "DAYS"
      object_name_filter = [
        {
          inclusion_patterns = ["my-test"]
        }
      ]
    },
    {
      name        = "Default2"
      action      = "ARCHIVE"
      is_enabled  = "true"
      time_amount = "12"
      time_unit   = "DAYS"
      object_name_filter = [
        {
          inclusion_patterns = ["my-test2"]
        }
      ]
    }
  ]
}
