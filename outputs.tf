output "bucket_id" {
  value = oci_objectstorage_bucket.this.id
}

output "bucket_name" {
  value = oci_objectstorage_bucket.this.name
}
